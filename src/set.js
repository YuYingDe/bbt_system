/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import md5 from 'js-md5';
import {accordion, panel, modal, alert} from 'vue-strap';   //各种组件,不依赖jquery
import hintComponent from '../component/hintView.vue';
var tabItems = [{name: '用户列表', style: 'active'},
    {name: '课程分类', style: ''},
    {name: '微信首页广告', style: ''},
    {name: '微信首页课程', style: ''}];
var messageTabs = [{name: '接口设置', style: 'btn-primary'}, {name: '群发消息', style: 'btn-default'}];
var _isClick = false;
var _isUserList = [];
var _isUserListUpdate = true;
var _isCourseType = [];
var _isCourseTypeUpdate = true;
var _isToken = [];
var _isTokenUpdata = true;
var _banner = [];
var _bannerUpdate = true;
var _wechatCourse = [];
var _wechatCourseUpdate = true;
var isClick = true;
module.exports = {
    data: function () {
        return {
            tabItems: tabItems,
            messageTabs: messageTabs,
            settingShow: 0,
            childShow: 0,
            showModal: false,
            showCustomModal: false,
            smallModal: false,
            editTypeModal: false,
            showPwdModal: false,
            showDeletedType: false,
            deletedTypeItem: '',
            user: '',
            isEdit: '',
            editItem: {
                name: '',userID: '',role: '', password: '', confirmPwd: ''
            },
            courseTypeItem: '',
            type: {
                name: '', typeID: '', rank: ''
            },
            editTypeItem: '',
            upload: {
                file: '', key: ''
            },
            alertShow: false,
            alertMSG: '',
            token: '',
            domain: 'http://res.bbtxsj.com/',
            imgSrc: '',
            imgName: '',
            banner: '',
            bannerItem: {name:'',url:'',selected: ''},
            showDeletedBannerModal: false,
            showDeletedBannerItem: '',
            wechatCourse: '',
            loadingHint: false,
            textMSG: '操作成功',
            showHint: false
        }
    },
    ready: function(){
        this.tabs(0);
    },
    components: {
        navbarComponent,
        accordion,
        panel,
        modal,
        alert,
        'hint-component': {
            template: hintComponent.template,
            props: {
                mes: ''
            }
        }
    },
    methods: {
        getToken(){
            var self = this;
            if(!_isTokenUpdata){
                self.token = _isToken;
                return;
            }
            _isToken = [];
            var parmas = {
                m: 'getToken'
            };
            this.$httpGet('qiniu', parmas, function(code, data){
                if(code == 0){
                    self.token = data.response;
                    _isToken = data.response;
                    _isTokenUpdata = false;
                }
            });
        },
        tabs(index){
            this.settingShow = index;
            if(index == 0){
                this.userList();
            }else if(index == 1){
                this.courseType();
            }else  if(index == 2){
                this.getToken();
                this.bannerList();
            }else if(index == 3){
                this.wechatCourseList();
            }
            tabItems.forEach(function(item){
                if(tabItems.indexOf(item)  == index){
                    item.style = 'active';
                }else{
                    item.style = '';
                }
            });
            this.tabItems = tabItems;
        },
        userList(){
            var self = this;
            if(!_isUserListUpdate){
                self.user = _isUserList;
                return;
            }
            _isUserList = [];
            var params = {
                m: 'list'
            };
            this.$httpGet('users', params, function(code, data){
                if(code == 0){
                    self.user = data.response;
                    _isUserList = data.response;
                    _isUserListUpdate = false;
                }
            });
        },
        edit(item){
            this.showModal = true;
            this.editItem = item;
            this.isEdit = true;
        },
        save(item, isEdit){
            if(!isClick){
                return;
            }
            isClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            var dataCheck = '';
            for(var i in item){
                if(item[i] === undefined  || item[i] == null || item[i] == ''){
                    dataCheck = false;
                }else{
                    dataCheck = true;
                }
            }
            if(dataCheck == false){
                alert('请确保信息填写完整！');
                return;
            }
            var params = {
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "account": item.userID,
                "name": item.name,
                "role": item.role
            };
            if(isEdit){
                params['m'] = 'update';
                params['platformID'] = item.platformID;
            }else{
                params['m'] = 'addUser';
                params['password'] = md5(item.password);
            }
            this.$httpPost('users', params, function(code, data){
                if(code == 0){
                    self.showModal = false;
                    _isUserListUpdate = true;
                    self.userList();
                }
                isClick = true;
            })
        },
        deleteUser(item){
            this.editItem = item;
            this.smallModal = true;
        },
        submit(item){
            //    delete user
            var self = this;
            var params = {
                m: 'remove',
                platformID: item.platformID
            };
            this.$httpGet('users', params, function(code, data){
                if(code == 0){
                    self.smallModal = false;
                    _isUserListUpdate = true;
                    self.userList();
                }
            });
        },
        addUser(){
            this.showModal = true;
            this.editItem = '';
            this.isEdit = false;
        },
        editPwd(item){
            this.editItem = item;
            this.showPwdModal = true;
        },
        savePwd(item){
            if(!isClick){
                return;
            }
            isClick = false;
            if(item.oldPwd == '' || item.oldPwd == '' || item.oldPwd === undefined){
                return;
            }
            if(item.newPwd == '' || item.newPwd == '' || item.newPwd === undefined){
                return;
            }
            var self = this;
            var params = {
                "m":"changePwd",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "platformID": item.platformID,
                "oldPassword": item.oldPwd,
                "password": item.newPwd
            };
            this.$httpPost('users', params, function(code, data){
                if(code == 0){
                    _isUserListUpdate = true;
                    self.userList();
                    self.showHint = true;
                    self.$nextTick(function(){
                       setTimeout(function(){
                           self.showHint = false;
                           self.showPwdModal = false;
                       }, 1500);
                    });
                }else{
                    self.textMSG = '操作失败'
                    self.showHint = true;
                    self.$nextTick(function(){
                        setTimeout(function(){
                            self.showHint = false;
                        }, 1500);
                    });
                }
                isClick = true;
            });
        },
        courseType(){
            var self = this;
            if(!_isCourseTypeUpdate){
                self.courseTypeItem = _isCourseType;
                return;
            }
            _isCourseType = [];
            var params = {
                m: 'list',
                type: 1
            };
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    result.forEach(function(item){
                        if(result.indexOf(item) == 0){
                            item.childShow = true;
                            item.class = 'glyphicon-menu-down';
                        }else{
                            item.childShow = false;
                            item.class = 'glyphicon-menu-up';
                        }
                    });
                    self.courseTypeItem = result;
                    _isCourseType = result;
                    _isCourseTypeUpdate = false;
                }
            });
        },
        showChild(index){
            console.log(index);
            var self = this;
            var result = _isCourseType;
            result.forEach(function(item){
                if(index == result.indexOf(item)){
                    if(item.childShow){
                        item.childShow = false;
                        item.class = 'glyphicon-menu-up';
                    }else{
                        item.childShow = true;
                        item.class = 'glyphicon-menu-down';
                    }
                }else{
                    item.childShow = false
                }
            });
            self.courseTypeItem = result;
        },
        //添加类型
        addType(typeItem){
            if(!isClick){
                return;
            }
            isClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            if(typeItem.name == null || typeItem.name == '' || typeItem.name === undefined){
                return;
            }
            if(typeItem.rank == null || typeItem.rank == '' || typeItem.rank === undefined){
                return;
            }
            if(typeItem.rank == 2){
                if(typeItem.typeID == '' || typeItem.typeID == null || typeItem.typeID === undefined){
                    return;
                }
            }
            var self = this;
            var params = {
                m: 'addClassification',
                userID: localStorage.getItem('userID'),
                session: localStorage.getItem('session'),
                type: typeItem.rank,
                name: typeItem.name
            };
            if(typeItem.rank == 2){
                params['parentID'] = typeItem.typeID;
            }
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                    _isCourseTypeUpdate = true;
                    self.courseType();
                }else{
                    alert('errorCode:' + code);
                }
                isClick = true;
            });
        },
        deleteShowModal(item){
            this.deletedTypeItem = item;
            this.showDeletedType = true;
        },
        //删除类型
        deleteType(id, type){
            if(!isClick){
                return;
            }
            isClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            var params = {
                m: 'remove',
                userID: localStorage.getItem('userID'),
                session: localStorage.getItem('session'),
                typeID: id,
                type: type
            };
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                    _isCourseTypeUpdate = true;
                    self.courseType();
                    self.showDeletedType = false;
                }else{
                    if(data.code == 13){
                        alert('不允许删除该分类');
                    }else if(data.code == 5){
                        alert('该分类已存在');
                    }
                }
                isClick = true;
            });
        },
        //编辑类型
        editType(item){
            this.editTypeItem = item;
            this.editTypeModal = true;
        },
        editTypeSubmit(item){
            if(!isClick){
                return;
            }
            isClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            var params = {
                m: 'update',
                userID: localStorage.getItem('userID'),
                session: localStorage.getItem('session'),
                typeID: item._id,
                name: item.name
            };
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                    _isCourseTypeUpdate = true;
                    self.editTypeModal = false;
                    self.courseType();
                } else{
                    alert('errorCode:' + code);
                }
                isClick = true;
            });
        },
        bannerList(){
            var self = this;
            if(!_bannerUpdate){
                self.banner = _banner;
                return
            }
            _banner = [];
            var params = {
                m: 'list',
                type: 3
            };
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                   self.banner = data.response;
                    _banner = data.response;
                    _bannerUpdate = false;

                } else{}
            });
        },
        addWechatBanner(){
            var self = this;
            this.bannerItem = '';
            this.imgName = '';
            this.imgSrc = '';
            this.showCustomModal = true;
            createUploader('pickfiles', 'container', _isToken, function(error, domain, res){
                if(error == 0){
                    self.imgName = res.key;
                    self.imgSrc = domain + res.key;
                }
            });
        },
        uploadSubmit(item, url){
            var self = this;
            if(!_isClick){
                var params = {
                    userID: localStorage.getItem('userID'),
                    session: localStorage.getItem('session'),
                    "name": item.name,
                    "image": url,
                    "url": item.url,
                    "number": item.number
                };
                if(item.advertID === undefined){
                    params['m'] = "addWCAdvert"
                }else{
                    params['m'] = "updateWCAdvert";
                    params['advertID'] = item.advertID;
                }
                _isClick = true;
                this.$httpPost('set', params, function(code, data){
                    if(code == 0){
                        self.showCustomModal = false;
                        _bannerUpdate = true;
                        _isClick = false;
                        self.bannerList();
                    }
                });
            }
        },
        showDeletedBanner(item){
            this.showDeletedBannerModal = true;
            this.showDeletedBannerItem = item;
        },
        deletedBanner(item){
            if(!_isClick){
                var self = this;
                var params = {
                    "m":"remove",
                    userID: localStorage.getItem('userID'),
                    session: localStorage.getItem('session'),
                    "advertID": item.advertID,
                    "type": 2
                };
                _isClick = true;
                this.$httpPost('set', params, function(code, data){
                    if(code == 0){
                        self.showDeletedBannerModal = false;
                        _bannerUpdate = true;
                        _isClick = false;
                        self.bannerList();
                    }
                });
            }
        },
        editBanner(item){
            var self = this;
            this.showCustomModal = true;
            this.bannerItem = item;
            this.imgName = '';
            this.imgSrc = item.image.url;
            createUploader('pickfiles', 'container', _isToken, function(error, domain, res){
                if(error == 0){
                    self.imgName = res.key;
                    self.imgSrc = domain + res.key;
                }
            });
        },
        wechatCourseList(){
            var self = this;
            if(!_wechatCourseUpdate){
                self.wechatCourse = _wechatCourse;
                return;
            }
            _wechatCourse = [];
            var params = {
                m: 'list',
                type: 2
            };
            this.$httpGet('set', params, function(code, data){
                if(code == 0){
                    self.wechatCourse = data.response;
                    _wechatCourse = data.response;
                    _wechatCourseUpdate = false;
                }
            });
        },
        pageCourse(item){
            this.loadingHint = true;
            var self = this;
            var params = {
                "m":"pageCourse",
                userID: localStorage.getItem('userID'),
                session: localStorage.getItem('session'),
                "typeID": item._id,
                "number": item.number
            };
            this.$httpGet('set', params, function(code, data){
               if(code == 0){
                   _wechatCourseUpdate = true;
                   self.wechatCourseList();
               }
                self.loadingHint = false;
            });
        },
        logout(){
            localStorage.clear();
            sessionStorage.clear();
            this.$router.go('/');
        },
        hintShow(mes){
            var self = this;
            this.alertMSG = mes;
            this.alertShow = true;
            this.$nextTick(function(){
                setTimeout(function(){
                    self.alertShow = false;
                }, 1500);
            })
        }

    }
};
