/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import moment from 'moment';
import {modal, popover} from 'vue-strap';
var tabItems = [{name: '客户列表', style: 'active', path: '/'}, {name: '学员列表', style: '', path: '/'}];
var _isCustomerDetails = '';
var _isCustomerDetailsUpdate = true;
var _isCustomerID = null;

export default {
    data() {
        return {
            items: {rTeacher:'',teacher: ''},
            tabItems: tabItems
        }
    },
    ready(){
        this.customerDetails();
    },
    components: {
        navbarComponent,
        modal,
        popover
    },
    methods: {
        tabs(index){
            tabItems.forEach(function(item){
                if(tabItems.indexOf(item)  == index){
                    item.style = 'active';
                }else{
                    item.style = '';
                }
            });
            this.tabItems = tabItems;
        },
        link(path){
          this.$router.go(path);
        },
        customerDetails(){
            var self = this;
            var customerID = this.$route.query.customerID;
            if(!_isCustomerDetailsUpdate && customerID == _isCustomerID){
                this.items = _isCustomerDetails;
                return;
            }
            _isCustomerDetails = '';
            var params = {
                m: 'info',
                type: 1,
                customerID: this.$route.query.customerID
            };
            this.$httpGet('customer', params, function(code, data){
                if(code == 0){
                    self.items = data.response;
                    _isCustomerDetails = data.response;
                    _isCustomerDetailsUpdate = false;
                    _isCustomerID = self.$route.query.customerID;
                }
            });
        }
    },
    filters: {
        'sexFilter': function(sex){
            if(sex == 0){
                return '女';
            }else{
                return '男'
            }
        },
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return ''
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        'statusFilter': function(status){
            //跟进状态, 0:初访 1:跟进中  2:已成交 3:已流失
            if(status == 0){
                return '初访'
            }else if(status == 1){
                return '跟进中'
            }else if(status == 2){
                return '已成交'
            }else if(status == 3){
                return '已流失'
            }
        }
    }
};
