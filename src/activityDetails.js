/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import loadingComponent from '../component/hintView.vue';
import {modal, popover} from 'vue-strap';
import moment from 'moment';
var tabItems = [{name: '活动管理', style: 'active'}];
var childTab = [{name: '基础信息', style: 'btn-primary'},
    {name: '报名人数', style: 'btn-default'}];

var _isActivity = [];
var _isActivityUpdate = true;
var _isActivityCount = [];
var _isActivityCountUpdate = true;
var _isActivityID = '';
var _isToken = [];
var _isTokenUpdata = true;
var _isImages = [];
var isEditClick = true;
var isClick = true;

export default {
    data: function () {
        return {
            tabItems: tabItems,
            childTab: childTab,
            actDetailsShow: 0,
            items: {headImage: '',images: []},
            showCustomModal: false,
            showDeletedModel: false,
            searchItem: '',
            options: [
                { text: '已下线', value: 0 },
                { text: '正在进行', value: 1 },
                { text: '报名中', value: 2 }
            ],
            refreshMes: '',
            isRefresh: false,
            showHeaderModel: false,
            photoID: '',
            isShowFiles: '',
            imgName: '',
            headImg: '',
            imagesItem: '',
            deletePhotoItem: '',
            token: '',
            domain: 'http://res.bbtxsj.com/'
        }
    },
    ready(){
      this.tabs(0);
    },
    components: {
        navbarComponent,
        "loading-component": {
            template: loadingComponent.template,
            props: {
                mes:''
            }
        },
        modal,
        popover
    },
    methods: {
        tabs(index){
            childTab.forEach(function(item){
                if(childTab.indexOf(item) == index){
                    item.style = 'btn-primary';
                } else{
                    item.style = 'btn-default';
                }
            });
            this.childTab = childTab;
            this.actDetailsShow = index
            if(index == 0){
               this.activityDetails();
            }else {
                this.participants(null);
            }
        },
        getToken(){
            var self = this;
            if(!_isTokenUpdata){
                self.token = _isToken;
                self.uploadHeadImg();
                self.uploadImages();
                self.uploadAddImages();
                return;
            }
            _isToken = [];
            var parmas = {
                m: 'getToken'
            };
            this.$httpGet('qiniu', parmas, function(code, data){
                if(code == 0){
                    self.token = data.response;
                    _isToken = data.response;
                    _isTokenUpdata = false;
                    self.uploadHeadImg();
                    self.uploadImages();
                    self.uploadAddImages();
                }
            });
        },
        activityDetails(){
            var self = this;
            var activityID = this.$route.query.activityID;
            if(activityID != _isActivityID){
                _isActivityCountUpdate = true;
            }
            if(!_isActivityUpdate && _isActivityID == activityID){
                self.items = _isActivity;
                return;
            }
            _isActivity = [];
            var params = {
                m: 'info',
                activityID: this.$route.query.activityID
            };
            this.$httpGet('activity', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    result['signUpTime'] = moment(new Date(result.signUpTime)).format("YYYY-MM-DD");
                    result['startTime'] = moment(new Date(result.startTime)).format("YYYY-MM-DD");
                    result['endTime'] = moment(new Date(result.endTime)).format("YYYY-MM-DD");
                    self.items = result;
                    _isActivity = result;
                    _isActivityUpdate = false;
                    _isActivityID = self.$route.query.activityID;
                } else{}
            });
        },
        participants(name){
            var self = this;
            if(!_isActivityCountUpdate){
                self.items = _isActivityCount;
                return;
            }
            _isActivityCount = [];
            var params = {
                m: 'list',
                type: 2,
                activityID: this.$route.query.activityID
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('activity', params, function(code, data){
                if(code == 0){
                    self.items = data.response;
                    _isActivityCount = data.response;
                    _isActivityCountUpdate = false;
                } else{}
            });
        },
        search(name){
            _isActivityCountUpdate = true;
            this.participants(name);
        },
        submit(item){
            if(!isClick){
                return;
            }
            isClick = false;
            this.isRefresh = true;
            this.refreshMes = '正在提交';
            var self = this;
            var desc = JSON.stringify(item.desc).replace(/\n/, "<br>");  // 获取文本域内所有换行符
            var params = {
                "m":"update",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "activityID": this.$route.query.activityID,
                "name": item.name,
                "address": item.address,
                "phone": item.phone,
                "price": item.price,
                "type": item.type,
                "signUpTime": item.signUpTime,    //报名截至日期
                "startTime": item.startTime,
                "endTime": item.endTime,
                "number": item.number, //最大人数,
                "status": item.status,
                "desc": desc
            };
            if(sessionStorage.getItem('geocode')){
                var geocode = JSON.parse(sessionStorage.getItem('geocode'));
                var lng = geocode[0].location.lng;
                var lat = geocode[0].location.lat;
                params['location'] = {
                    "longitude": lng,
                    "latitude": lat
                }
            }
            console.log(params);
            this.$httpPost('activity', params, function (code, data) {
                if(code == 0){
                    self.showCustomModal = false;
                    _isActivityUpdate = true;
                    self.activityDetails();
                    self.isRefresh = false;
                }else{
                    alert('error!')
                }
                isClick = true;
            });
        },
        editHeadImage(type, id){
            uploader = null;
            this.getToken();
            this.isShowFiles = type;
            this.showHeaderModel = true;
            this.photoID = id;
        },
        uploadHeadImg(){
            var self = this;
            createUploader('headfiles', 'newContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    self.imgName = res.key;
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
                uploader = null;
            });
        },
        uploadImages(){
            var self = this;
            createUploader('pickfiles', 'editContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    self.imgName = res.key;
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
                uploader = null;
            });
        },
        uploadAddImages(){
            var self = this;
            createUploader('addfiles', 'addContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    if(_isImages.length == 0){
                        var img = [];
                        img.push(domain + res.key);
                        _isImages = img;
                        self.headImg = img;
                    }else{
                        _isImages.push(domain + res.key);
                        self.headImg = _isImages;
                        console.log(_isImages);
                    }
                }else if(code == 1) {
                    console.log(domain);
                }
                uploader = null;
            });
        },
        submitEdit(type, item, image){
            if(!isEditClick){
                return;
            }
            isEditClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            if(image == null || image == '' || image.length == 0 || image === undefined){
                alert('你还没有上传图片！');
                return;
            }
            var params = {
                "m": "update",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "activityID": this.$route.query.activityID
            };
            if(type == 0){
                params['headImage'] = image;
            }else if(type == 1){
                var id = this.photoID;
                var imageFiles = item.images;
                imageFiles.forEach(function(obj){
                    if(obj.photoID == id){
                        obj.url = image;
                        delete obj.photoID;
                    }
                });
                params['images'] = imageFiles;
            }else if(type == 2){
                var addfiles = [];
                var arr = item.images;
                _isImages.forEach(function(item){
                    addfiles.push({url: item});
                });
                params['images'] = addfiles.concat(arr);
            }
            this.$httpPost('activity', params, function(code, data){
                if(code == 0){
                    _isActivityUpdate = true;
                    self.imgName = '';
                    self.headImg = '';
                    self.activityDetails();
                    self.showHeaderModel = false;
                }
                isEditClick = true;
            });
        },
        showDeletePhoto(item){
            this.deletePhotoItem = item;
            this.showDeletedModel = true;
        },
        deletedPhoto(item){
            var self = this;
            if(!isClick){
                return;
            }
            isClick = false;
            var params = {
                "m":"removeImages",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "activityID": this.$route.query.activityID,
                "images":[item.photoID]
            };
            this.$httpPost('activity', params, function(code, data){
                alert('删除成功！');
                _isActivityUpdate = true;
                self.activityDetails();
                self.showDeletedModel = false;
                isClick = true;
            });
        }
    },
    filters: {
        'typeFilter': function(type){
            if(type == null || type === undefined){
                return '暂无类型';
            }else{
                //活动类型 赛事、户外体验、商演、活动
                if(type == 1){
                    return '赛事';
                }else if(type == 2){
                    return '户外体验';
                }else if(type == 3){
                    return '商业表演';
                }else if(type == 4){
                    return '活动';
                }
            }
        },
        'statusFilter': function(status){
            if(status == null || status === undefined){
                return '暂无状态';
            }else {
                if(status == 0){
                    return '已下线';
                }else if(status == 1){
                    return '正在进行'
                }else if(status == 2){
                    return '报名中';
                }
            }
        },
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return '- -';
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        'imageFilter': function(image){
            return image + '?imageView2/1/w/150/h/70';
        },
        "descFilter": function(desc){
            if(desc == null || desc == ''){
                return '';
            }else{
                var str = desc.replace(/<br\s*\/?>/g, "");
                return str;
            }
        }
    }
};
