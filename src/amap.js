/**
 * Created by YYD on 5/22/16.
 */
var map = new AMap.Map("container", {
    resizeEnable: true
});
module.exports = {
    data: function () {
        return {

        }
    },
    ready(){
        this.geocoder();
    },
    methods: {
        geocoder() {
            var self = this;
            map.plugin(["AMap.ToolBar"], function() {
                map.addControl(new AMap.ToolBar());
            });
            var geocoder = new AMap.Geocoder({
                city: "", //城市，默认：“全国”
                radius: 900 //范围，默认：500
            });
            //地理编码,返回地理编码结果
            geocoder.getLocation("广州市华软软件学院", function(status, result) {
                if (status === 'complete' && result.info === 'OK') {
                    self.geocoder_CallBack(result);
                }
            });
        },
        addMarker(i, d) {
            var marker = new AMap.Marker({
                map: map,
                position: [ d.location.getLng(),  d.location.getLat()]
            });
            var infoWindow = new AMap.InfoWindow({
                content: d.formattedAddress,
                offset: {x: 0, y: -30}
            });
            marker.on("mouseover", function(e) {
                infoWindow.open(map, marker.getPosition());
            });
        },
        geocoder_CallBack(data) {
            var self = this;
            var resultStr = "";
            //地理编码结果数组
            var geocode = data.geocodes;
            for (var i = 0; i < geocode.length; i++) {
                //拼接输出html
                resultStr += "<span style=\"font-size: 12px;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\">" + "<b>地址</b>：" + geocode[i].formattedAddress + "" + "&nbsp;&nbsp;<b>的地理编码结果是:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;坐标</b>：" + geocode[i].location.getLng() + ", " + geocode[i].location.getLat() + "" + "<b>&nbsp;&nbsp;&nbsp;&nbsp;匹配级别</b>：" + geocode[i].level + "</span>";
                self.addMarker(i, geocode[i]);
            }
            map.setFitView();
            document.getElementById("result").innerHTML = resultStr;
        }
    }
};