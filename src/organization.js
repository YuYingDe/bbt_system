/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import loadingComponent from '../component/hintView.vue';
import md5 from 'js-md5';
import {modal} from 'vue-strap';   //各种组件,不依赖jquery
import moment from 'moment';
var tabItems = [{name: '机构信息', style: 'active'},{name: '审核机构', style: ''},{name: '添加机构', style: ''}];
var _isOrganization = [];
var _isApply = [];
var _isOrganizationUpdate = true;
var _isApplyUpdate = true;
var _isImages = [];
var _isToken = '';
var _isTokenUpdata = true;
var isClick = true;
module.exports = {
    data() {
        return {
            tabItems: tabItems,
            orgItems: {},
            searchItem: '',
            showCustomModal: false,
            checkModal: false,
            changPwdModal: false,
            checkModalItem: {
                cityname: '深圳'  //default city
            },
            changePwdItem: '',
            isOrgList: 0,
            refreshMes: '正在刷新..',
            isRefresh: false,
            isAddOrg: false,
            headImg: '',
            imgFiles: '',
            imgName: '',
            imagesItem: [],
            defaultImg: '../image/demo.jpg',
            token: '',
            domain: 'http://res.bbtxsj.com/'
        }
    },
    ready(){
        _init_area();   //js实现全国三级城市联动select选择
        this.tabs(0);
    },
    components: {
        navbarComponent,
        modal,
        "loading-component": {
            template: loadingComponent.template,
            props: {
                mes:''
            }
        }
    },
    methods: {
        getToken(){
            var self = this;
            if(!_isTokenUpdata){
                self.token = _isToken;
                return;
            }
            _isToken = [];
            var parmas = {
              m: 'getToken'
            };
            this.$httpGet('qiniu', parmas, function(code, data){
               if(code == 0){
                   self.token = data.response;
                   _isToken = data.response;
                   _isTokenUpdata = false;
                   self.uploadImages();
                   self.uploadHeadImg();
               }
            });
        },
        organizationList(page, name){
            var self = this;
            if(!_isOrganizationUpdate){
                self.orgItems = _isOrganization;
                return;
            }
            _isOrganization = [];
            var params = {
                m: 'list',
                type: 1,
                fetchCount: 10,
                page: page
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.orgItems = result;
                    _isOrganization = result;
                    _isOrganizationUpdate = false;
                    setTimeout(function(){
                        self.isRefresh = false;
                    },1500);
                }else{}
            });
        },
        applyList(page, name){
            var self = this;
            if(!_isApplyUpdate){
                self.orgItems = _isApply;
                return;
            }
            _isApply = [];
            var params = {
                m: 'list',
                type: 6,
                fetchCount: 10,
                page: page
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.orgItems = result;
                    _isApply = result;
                    _isApplyUpdate = false;
                    setTimeout(function(){
                        self.isRefresh = false;
                    },1500);
                }else{}
            });
        },
        tabs(index){
            if(index == 0){
                this.organizationList(0, null);
                this.isOrgList = index;
            }else if(index == 1){
                this.applyList(0, null);
                this.isOrgList = index;
            }else if(index == 2){
                uploader = null;
                this.getToken();
                this.checkModalItem = '';
                this.imgName = '';
                this.headImg = '';
                this.imagesItem = '';
                this.isAddOrg = true;
                this.checkModal = true;
            }
            tabItems.forEach(function(item){
                if(tabItems.indexOf(item)  == index){
                    item.style = 'active';
                }else{
                    item.style = '';
                }
            });
            this.tabItems = tabItems;
        },
        search(name, isShow){
            if(isShow == 0){
                console.log(isShow + '\n' + name);
                _isOrganizationUpdate = true;
                this.organizationList(0, name);
            }else if(isShow == 1){
                _isApplyUpdate = true;
                this.applyList(0, name);
            }
        },
        refresh(isOrgList){
            this.isRefresh = true;
            if(isOrgList == 0){
                _isOrganizationUpdate = true;
                this.organizationList(0, null);
            }else{
                _isApplyUpdate = true;
                this.applyList(0, null);
            }
        },
        next(fenye, item, isShow){
            var pageNumber = item.page;
            var pageCount = item.pageCount;
            if(fenye == 'Next'){
                if(pageCount > pageNumber + 1){
                    if(isShow == 0){
                        _isOrganizationUpdate = true;
                        this.organizationList(pageNumber + 1, null);
                    }else{
                        _isApplyUpdate = true;
                        this.a(pageNumber + 1, null);
                    }
                }
            }else if(fenye == 'Previous'){
                if(pageNumber > 0){
                    if(isShow == 0){
                        _isOrganizationUpdate = true;
                        this.organizationList(pageNumber - 1, null);
                    }else{
                        _isApplyUpdate = true;
                        this.applyList(pageNumber - 1, null);
                    }
                }
            }else if(fenye == 'pageNum'){
                if(isShow == 0){
                    _isOrganizationUpdate = true;
                    this.organizationList(item, null);
                }else{
                    _isApplyUpdate = true;
                    this.applyList(item, null);
                }
            }
        },
        check(item){
            this.checkModalItem = item;
            this.checkModal = true;
            this.getToken();
        },
        uploadHeadImg(){
            var self = this;
            createUploader('headfiles', 'container', _isToken, function(code, domain, res){
                if(code == 0){
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
            });
        },
        uploadImages(){
            var self = this;
            createUploader('pickfiles', 'fileContainner', _isToken, function(code, domain, res){
                if(code == 0){
                    _isImages.push(domain + res.key);
                    self.imagesItem = _isImages;
                }else if(code == 1){
                    console.log(domain);
                }
            });
        },
        submit(items, img, images, isOrgList){
            if(!isClick){
               return;
            }
            isClick = false;
            this.refreshMes = '正在提交';
            this.isRefresh = true;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            var dataCheck = '';
            for(var i in items){
                if(items[i] === undefined  || items[i] == null || items[i] == ''){
                    dataCheck = false;
                }else{
                    dataCheck = true;
                }
            }
            if(img == '' || img == null || img === undefined){
                alert('请上传机构封面图！');
                return;
            }
            if(dataCheck == false){
                alert('请确保信息填写完整！');
                return;
            }
            if(images == '' || images == null || images === undefined || images.length == 0){
               alert('请上传图片组！');
                return;
            }
            var desc = items.desc.replace(/\n/g, "<br/>");
            var orgAddress = items.province + items.city + items.county + items.address;
            console.log(orgAddress);
            this.$amapGeocode(orgAddress, items.city, function(code, data){
                var location = data.location;
                var i = location.indexOf(",");
                var lng = location.substring(0, i);
                var lat = location.substring(i + 1);
                //var lng  = data.longitude;
                //var lat = data.latitude;
                var params = {
                    "m": "addOrg",
                    "contact": items.contact,
                    "name": items.name,
                    "password": md5(items.password),
                    "phone": items.phone,
                    "orgAge": items.orgAge,
                    "desc": desc,
                    "address": orgAddress,
                    "location": {
                        "longitude": lng,
                        "latitude": lat
                    },
                    "userID": localStorage.getItem('userID'),
                    "session": localStorage.getItem('session'),
                    "headImage": img,
                    "images": images,
                    "inside": items.inside
                };
                self.$httpPost('org', params, function (code, data) {
                    if(code == 0){
                        self.checkModal = false;
                        self.headImg = '';
                        self.imagesItem = '';
                        self.checkModalItem = '';
                        uploader = null;
                        _isImages = [];
                        tabItems.forEach(function(item){
                            if(tabItems.indexOf(item)  == 0){
                                item.style = 'active';
                            }else{
                                item.style = '';
                            }
                        });
                        self.isOrgList = 0;
                        self.tabItems = tabItems;
                        _isApplyUpdate = true;
                        _isOrganizationUpdate = true;
                        if(isOrgList == 0){
                            self.organizationList(0, null);
                        }else{
                            self.applyList(0, null);
                        }
                        self.isRefresh = false;
                    }else{
                        alert('error!')
                    }
                    isClick = true;
                });
            });
        },
        cancel(){
            this.checkModalItem = '';
            this.headImg = '';
            this.imagesItem = '';
            this.checkModal = false;
        },
        showChangePwd(item){
            this.changePwdItem = item;
            this.changPwdModal = true;
        },
        changePwd(item){
            if(item.newPwd == '' || item.newPwd == null || item.newPwd === undefined){
                return;
            }
            if(!isClick){
                return;
            }
            isClick = false;
            var self = this;
            var params = {
                m: 'changePwd',
                orgID: item.orgID,
                password: md5(item.newPwd),
                userID: localStorage.getItem('userID'),
                session: localStorage.getItem('session')
            };
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    alert('修改成功！');
                    _isOrganizationUpdate = true;
                    self.organizationList(0, null);
                    self.changPwdModal = false;
                }
                isClick = true;
            })
        }
    },
    filters: {
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return 'null'
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        'imageFilter': function(img){
            if(img === undefined || img == '' || img == null){
                return '<i class="glyphicon glyphicon-picture"></i>';
            }else{
                return '<img src="' + img +'" />'
            }
        },
        'imgFilter': function(image){
            return image + '?imageView2/1/w/120/h/50'
        }
    }
};
