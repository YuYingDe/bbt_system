/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';

var tabItems = [{name: '统计管理', style: 'active'}];

module.exports = {
    data: function () {
        return {
            tabItems: tabItems
        }
    },
    components: {
        navbarComponent
    },
    methods: {

    }
};
