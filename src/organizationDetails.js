/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import moment from 'moment';
import {modal, popover} from 'vue-strap';
var tabItems = [{name: '机构信息', style: 'active', link: '/org'},{name: '审核机构', style: '', link: '/org'}];
var childTab = [{name: '基础信息', style: 'btn-primary'},
    {name: '课程列表', style: 'btn-default'},
    {name: '师资列表', style: 'btn-default'},
    {name: '客户列表', style: 'btn-default'},
    {name: '学员列表', style: 'btn-default'}];
var _isOrgID = [];
var _isOrgDetail = [];
var _isOrgDetailUpdate = true;
var _isCourse = [];
var _isCourseUpdate = true;
var _isTeacher = [];
var _isTeacherUpdate = true;
var _isCustomer = [];
var _isCustomerUpdate =  true;
var _isStudent = [];
var _isStudentUpdate = true;
var _isToken = [];
var _isTokenUpdata = true;
var _isImages = [];
var isClick = true;
var isEditClick = true;
export default {
    data() {
        return {
            tabItems: tabItems,
            childTab: childTab,
            orgDetailsShow: 0,
            items: {headImage: ''},
            searchItem: '',
            showCustomModal: false,
            showHeaderModel: false,
            showDeletedModel: false,
            photoID: '',
            isShowFiles: '',
            imgName: '',
            headImg: '',
            imagesItem: '',
            deletePhotoItem: '',
            notData: false,
            token: '',
            domain: 'http://res.bbtxsj.com/',
            isUploading: false
        }
    },
    ready(){
      this.tabs(0);
    },
    components: {
        navbarComponent,
        modal,
        popover
    },
    methods: {
        getToken(){
            var self = this;
            if(!_isTokenUpdata){
                self.token = _isToken;
                self.uploadHeadImg();
                self.uploadImages();
                self.uploadAddImages();
                return;
            }
            _isToken = [];
            var parmas = {
                m: 'getToken'
            };
            this.$httpGet('qiniu', parmas, function(code, data){
                if(code == 0){
                    self.token = data.response;
                    _isToken = data.response;
                    _isTokenUpdata = false;
                    self.uploadHeadImg();
                    self.uploadImages();
                    self.uploadAddImages();
                }
            });
        },
        organizationDetail(){
            var self = this;
            var orgID = this.$route.query.orgID;
            if(_isOrgID != orgID){
                _isCourseUpdate = true;
                _isTeacherUpdate = true;
                _isCustomerUpdate =  true;
                _isStudentUpdate = true;
            }
            if(!_isOrgDetailUpdate && _isOrgID == orgID){
                self.items = _isOrgDetail;
                return;
            }
            _isOrgDetail = [];
            var params = {
                m: 'info',
                orgID: this.$route.query.orgID
            };
            this.$httpGet('org', params, function(code, data){
               if(code == 0){
                   var result = data.response;
                   //result['desc'] = result.desc.replace(/<br\s*\/?>/g, "");
                   self.items = result;
                   _isOrgDetail = result;
                   _isOrgDetailUpdate = false;
                   _isOrgID = self.$route.query.orgID;
               } else{}
            });
        },
        courseList(page, name){
            var self = this;
            if(!_isCourseUpdate){
                self.items = _isCourse;
                if(_isCourse.datas.length == 0){
                    self.notData = true;
                }
                return;
            }
            _isCourse = [];
            var params = {
                m: 'list',
                type: 2,
                fetchCount:10,
                page: page,
                orgID: this.$route.query.orgID
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.items = result;
                    _isCourse = result;
                    if(_isCourse.datas.length == 0){
                        self.notData = true;
                    }
                    _isCourseUpdate = false;
                } else{}
            });
        },
        teacherList(page, name){
            var self = this;
            if(!_isTeacherUpdate){
                self.items = _isTeacher;
                if(_isTeacher.datas.length == 0){
                    self.notData = true;
                }
                return;
            }
            _isTeacher = [];
            var params = {
                m: 'list',
                type: 3,
                fetchCount:10,
                page: page,
                orgID: this.$route.query.orgID
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.items = result;
                    _isTeacher = result;
                    if(_isTeacher.datas.length == 0){
                        self.notData = true;
                    }
                    _isTeacherUpdate = false;
                } else{}
            });
        },
        customerList(page, name){
            var self = this;
            if(!_isCustomerUpdate){
                self.items = _isCustomer;
                if(_isCustomer.datas.length == 0){
                    self.notData = true;
                }
                return;
            }
            _isCustomer = [];
            var params = {
                m: 'list',
                type: 4,
                fetchCount:10,
                page: page,
                orgID: this.$route.query.orgID
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.items = result;
                    _isCustomer = result;
                    if(_isCustomer.datas.length == 0){
                        self.notData = true;
                    }
                    _isCustomerUpdate = false;
                } else{}
            });
        },
        studentList(page, name){
            var self = this;
            if(!_isStudentUpdate){
                self.items = _isStudent;
                if(_isStudent.datas.length == 0){
                    self.notData = true;
                }
                return;
            }
            _isStudent = [];
            var params = {
                m: 'list',
                type: 5,
                fetchCount:10,
                page: page,
                orgID: this.$route.query.orgID
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('org', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.items = result;
                    _isStudent = result;
                    if(_isStudent.datas.length == 0){
                        self.notData = true;
                    }
                    _isStudentUpdate = false;
                } else{}
            });
        },
        link(path){
            this.$router.go(path)
        },
        tabs(index){
            if(index == 0){
                this.organizationDetail();
            }else if(index == 1){
                this.courseList(0, null);
            }else if(index == 2){
                this.teacherList(0, null);
            }else if(index == 3){
                this.customerList(0, null);
            }else if(index == 4){
                this.studentList(0, null);
            }
            childTab.forEach(function(item){
               if(childTab.indexOf(item) == index){
                   item.style = 'btn-primary';
               } else{
                   item.style = 'btn-default';
               }
            });
            this.childTab = childTab;
            this.orgDetailsShow = index
        },
        search(name, isShow){
            if(isShow == 1){
                _isCourseUpdate = true;
                this.courseList(0, name);
            }else if(isShow == 2){
                _isTeacherUpdate = true;
                this.teacherList(0, name);
            }else if(isShow == 3){
                _isCustomerUpdate = true;
                this.customerList(0, name);
            }else if(isShow == 4){
                _isStudentUpdate = true;
                this.studentList(0, name);
            }
        },
        submit(items){
            if(!isClick){
                return;
            }
            isClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var desc = items.desc.replace(/\n/g, "<br/>");
            var self = this;
            var params = {
                "m": "update",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "orgID": this.$route.query.orgID,
                "contact": items.contact,
                "name": items.name,
                "phone": items.phone,
                "address": items.address,
                "orgAge": items.orgAge,
                "desc": desc
            };
            if(sessionStorage.getItem('geocode')){
                var geocode = JSON.parse(sessionStorage.getItem('geocode'));
                var lng = geocode[0].location.lng;
                var lat = geocode[0].location.lat;
                params['location'] = {
                    "longitude": lng,
                    "latitude": lat
                }
            }
            this.$httpPost('org', params, function (code, data) {
               if(code == 0){
                   self.showCustomModal = false;
                   _isOrgDetailUpdate = true;
                   self.organizationDetail();
               }else{
                   alert('error!')
               }
                isClick = true;
            });
        },
        next(fenye, item, isShow){
            var pageNumber = item.page;
            var pageCount = item.pageCount;
            if(fenye == 'Next'){
                if(pageCount > pageNumber + 1){
                    if(isShow == 1){
                        _isCourseUpdate = true;
                        this.courseList(pageNumber + 1, null);
                    }else if(isShow == 2){
                        _isTeacherUpdate = true;
                        this.teacherList(pageNumber + 1, null);
                    }else if(isShow == 3){
                        _isCustomerUpdate = true;
                        this.customerList(pageNumber + 1, null);
                    }else if(isShow == 4){
                        _isStudentUpdate = true;
                        this.studentList(pageNumber + 1, null);
                    }
                }
            }else if(fenye == 'Previous'){
                if(pageNumber > 0){
                    if(isShow == 1){
                        _isCourseUpdate = true;
                        this.courseList(pageNumber - 1, null);
                    }else if(isShow == 2){
                        _isTeacherUpdate = true;
                        this.teacherList(pageNumber - 1, null);
                    }else if(isShow == 3){
                        _isCustomerUpdate = true;
                        this.customerList(pageNumber - 1, null);
                    }else if(isShow == 4){
                        _isStudentUpdate = true;
                        this.studentList(pageNumber - 1, null);
                    }
                }
            }else if(fenye == 'pageNum'){
                if(isShow == 1){
                    _isCourseUpdate = true;
                    this.courseList(item, null);
                }else if(isShow == 2){
                    _isTeacherUpdate = true;
                    this.teacherList(item, null);
                }else if(isShow == 3){
                    _isCustomerUpdate = true;
                    this.customerList(item, null);
                }else if(isShow == 4){
                    _isStudentUpdate = true;
                    this.studentList(item, null);
                }
            }
        },
        editHeadImage(type, id){
            this.getToken();
            this.isShowFiles = type;
            this.photoID = id;
            this.showHeaderModel = true;
        },
        uploadHeadImg(){
            var self = this;
            createUploader('headfiles', 'newContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    self.imgName = res.key;
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
                uploader = null;
            });
        },
        uploadImages(){
            var self = this;
            createUploader('pickfiles', 'editContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    self.imgName = res.key;
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
                uploader = null;
            });
        },
        uploadAddImages(){
            var self = this;
            createUploader('addfiles', 'addContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    if(_isImages.length == 0){
                        var img = [];
                        img.push(domain + res.key);
                        _isImages = img;
                        self.headImg = img;
                    }else{
                        _isImages.push(domain + res.key);
                        self.headImg = _isImages;
                    }
                }else if(code == 1) {
                    console.log(domain);
                }
                uploader = null;
            });
        },
        submitEdit(type, item, image){
            if(!isEditClick){
                return;
            }
            isEditClick = false;
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                this.$router.go('/login');
                return;
            }
            var self = this;
            if(image == null || image == '' || image.length == 0 || image === undefined){
                alert('你还没有上传图片！');
                return;
            }
            var params = {
                "m": "update",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "orgID": this.$route.query.orgID
            };
            if(type == 0){
                params['headImage'] = image;
            }else if(type == 1){
                var id = this.photoID;
                var imageFiles = item.images;
                imageFiles.forEach(function(obj){
                   if(obj.photoID == id){
                       obj.url = image;
                       delete obj.photoID;
                   }
                });
                params['images'] = imageFiles;
            }else if(type == 2){
                var addfiles = [];
                var arr = item.images;
                _isImages.forEach(function(item){
                    addfiles.push({url: item});
                });
                params['images'] = addfiles.concat(arr);
            }
            this.$httpPost('org', params, function(code, data){
                uploader = null;
                if(code == 0){
                    _isOrgDetailUpdate = true;
                    self.imgName = '';
                    self.headImg = '';
                    self.organizationDetail();
                    self.showHeaderModel = false;
                }
                isEditClick = true;
            });
        },
        showDeletePhoto(item){
            this.deletePhotoItem = item;
            this.showDeletedModel = true;
        },
        deletedPhoto(item){
            var self = this;
            if(!isClick){
                return;
            }
            isClick = false;
            var params = {
                "m":"removeImages",
                "userID": localStorage.getItem('userID'),
                "session": localStorage.getItem('session'),
                "orgID": this.$route.query.orgID,
                "images":[item.photoID]
            };
            this.$httpPost('org', params, function(code, data){
                alert('删除成功！');
                _isOrgDetailUpdate = true;
                self.organizationDetail();
                self.showDeletedModel = false;
                isClick = true;
            });
        }
    },
    filters: {
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return 'null'
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        "descFilter": function(desc){
            if(desc == null || desc == ''){
                return '';
            }else{
                var str = desc.replace(/<br\s*\/?>/g, "");
                return str;
            }
        },
        'addressFilter': function(address){
            return '<iframe src="../amap.html?address=' + address + '" width="100%" height="300px" style="border-style: none"></iframe>'
        },
        'imageFilter': function(image){
            return image + '?imageView2/1/w/150/h/70';
        }
    }
};
