/**
 * Created by YYD on 5/6/16.
 */
"use strict";
import navbarComponent from '../component/navbarComponent.vue';
import Vue from 'vue';
import md5 from 'js-md5';
import mkoService from '../dist/mko-service';
Vue.use(mkoService);
module.exports = {
    data: function () {
        return {
            item: {
                name: '',
                pwd: ''
            },
            error: false,
            message: ''
        }
    },
    ready: function(){
        //console.log(Strap)
    },
    components: {
        navbarComponent
    },
    methods: {
        login(item){
            var self = this;
            var params = {
                m: 'login',
                userID: item.name,
                password: md5(item.pwd)
            };
            console.log(params);
            this.$httpGet('users', params, function (code, data) {
                if (code == 0) {
                    console.log(data);
                    localStorage.setItem('session', data.response.session);
                    localStorage.setItem('userID', data.response.user.userID);
                    self.$router.go('/');
                } else {
                    self.hintView('登录失败！');
                }
            });
        },
        hintView(mes){
            var self = this;
            this.error = true;
            this.message = mes;
            this.$nextTick(function(){
               setTimeout(function(){
                   self.error = false;
               }, 2000);
            });
        }

    }
};
