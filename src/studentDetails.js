/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import moment from 'moment';
import {modal, popover} from 'vue-strap';
var tabItems = [{name: '客户列表', style: '', path: '/'}, {name: '学员列表', style: 'active', path: '/'}];
var _isCustomerDetails = '';
var _isCustomerDetailsUpdate = true;
var _isCustomerID = null;

export default {
    data() {
        return {
            items: {rTeacher:'',teacher: ''},
            tabItems: tabItems
        }
    },
    ready(){
        this.customerDetails();
    },
    components: {
        navbarComponent,
        modal,
        popover
    },
    methods: {
        tabs(index){
            tabItems.forEach(function(item){
                if(tabItems.indexOf(item)  == index){
                    item.style = 'active';
                }else{
                    item.style = '';
                }
            });
            this.tabItems = tabItems;
        },
        link(path){
            this.$router.go(path);
        },
        customerDetails(){
            var self = this;
            var studentID = this.$route.query.studentID;
            if(!_isCustomerDetailsUpdate && studentID == _isCustomerID){
                this.items = _isCustomerDetails;
                return;
            }
            _isCustomerDetails = '';
            var params = {
                m: 'info',
                type: 2,
                studentID: this.$route.query.studentID
            };
            this.$httpGet('customer', params, function(code, data){
                if(code == 0){
                    self.items = data.response;
                    _isCustomerDetails = data.response;
                    _isCustomerDetailsUpdate = false;
                    _isCustomerID = self.$route.query.studentID;
                }
            });
        }
    },
    filters: {
        'sexFilter': function(sex){
            if(sex == 0){
                return '女';
            }else{
                return '男'
            }
        },
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return ''
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        }
    }
};
