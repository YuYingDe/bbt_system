/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import loadingComponent from '../component/hintView.vue';
import moment from 'moment';
var tabItems = [{name: '客户列表', style: 'active', isXueyuan: false},
    {name: '学员列表', style: '', isXueyuan: true}];

var _isCustomer = [];
var _isCustomerUpdate = true;
var _isStudent = [];
var _isStudentUpdate = true;
module.exports = {
    data: function () {
        return {
            customerItems: '',
            searchItem: '',
            tabItems: tabItems,
            isXueyuan: 0,
            refreshMes: '正在刷新',
            isRefresh: false
        }
    },
    ready(){
      this.tabs(0);
    },
    components: {
        navbarComponent,
        "loading-component": {
            template: loadingComponent.template,
            props: {
                mes:''
            }
        }
    },
    methods: {
        customerList(page, name){
            var self = this;
            if(!_isCustomerUpdate){
                self.customerItems = _isCustomer;
                return;
            }
            _isCustomer = [];
            var params = {
                m: 'list',
                fetchCount: 10,
                page: page,
                type: 1
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('customer', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.customerItems = result;
                    _isCustomer = result;
                    _isCustomerUpdate = false;
                    setTimeout(function(){
                        self.isRefresh = false;
                    },1500);
                }else{}
            });
        },
        studenList(page, name){
            var self = this;
            if(!_isStudentUpdate){
                self.customerItems = _isStudent;
                return;
            }
            _isStudent = [];
            var params = {
                m: 'list',
                fetchCount: 10,
                page: page,
                type: 2
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('customer', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.customerItems = result;
                    _isStudent = result;
                    _isStudentUpdate = false;
                    setTimeout(function(){
                        self.isRefresh = false;
                    },1500);
                }else{}
            });
        },
        tabs(index){
            if(index == 0){
                this.customerList(0, null);
            }else if(index == 1){
                this.studenList(0, null);
            }
            tabItems.forEach(function(item){
                if(tabItems.indexOf(item)  == index){
                    item.style = 'active';
                }else{
                    item.style = '';
                }
            });
            this.tabItems = tabItems;
            this.isXueyuan = index;
        },
        search(name, isShow){
            if(isShow == 0){
                _isCustomerUpdate = true;
                this.customerList(0, name);
            }else if(isShow == 1){
                _isStudentUpdate = true;
                this.studenList(0, name);
            }
        },
        refresh(isXueyuan){
            this.isRefresh = true;
            if(isXueyuan == 0){
                _isCustomerUpdate = true;
                this.customerList(0, null);
            }else if(isXueyuan == 1){
                _isStudentUpdate = true;
                this.studenList(0, null);
            }
        },
        next(fenye, item, isShow){
            var pageNumber = item.page;
            var pageCount = item.pageCount;
           if(fenye == 'Next'){
               if(pageCount > pageNumber + 1){
                   if(isShow == 0){
                       _isCustomerUpdate = true;
                       this.customerList(pageNumber + 1, null);
                   }else{
                       _isStudentUpdate = true;
                       this.studenList(pageNumber + 1, null);
                   }
               }
           }else if(fenye == 'Previous'){
               if(pageNumber > 0){
                   if(isShow == 0){
                       _isCustomerUpdate = true;
                       this.customerList(pageNumber - 1, null);
                   }else{
                       _isStudentUpdate = true;
                       this.studenList(pageNumber - 1, null);
                   }
               }
           }else if(fenye == 'pageNum'){
               if(isShow == 0){
                   _isCustomerUpdate = true;
                   this.customerList(item, null);
               }else{
                   _isStudentUpdate = true;
                   this.studenList(item, null);
               }
           }
        }
    },
    filters: {
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return ''
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        'nameFilter': function(item){
            if(item == null || item === undefined){
                return '';
            }else{
                return item.name;
            }
        },
        'phoneFilter': function(item){
            if(item == null || item === undefined){
                return '';
            }else{
                return item.phone;
            }
        },
        'headImageFilter': function(item){
            if(item == null || item === undefined){
                return '';
            }else{
                return item.url;
            }
        },
        'imageFilter': function(image){
            if(image == '' || image == null || image === undefined){
                return '../image/bbt.ico?imageView2/1/w/50/h/50';
            }else{
                return image + '?imageView2/1/w/50/h/50';
            }
        }
    }
};
