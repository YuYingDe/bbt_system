/**
 * Created by YYD on 5/6/16.
 */
"use strict";

import navbarComponent from '../component/navbarComponent.vue';
import loadingComponent from '../component/hintView.vue';
import moment from 'moment';
import {modal} from 'vue-strap';
var tabItems = [{name: '活动管理', style: 'active'}];
var toDay = moment(new Date()).format("YYYY-MM-DD");
var _isActivity = [];
var _isActivityUpdate = true;
var _isToken = '';
var _isTokenUpdata = true;
var _isImages = [];
var isClick = true;
module.exports = {
    data: function () {
        return {
            tabItems: tabItems,
            act: '',
            items: {
                signUpTime: toDay,
                startTime: toDay,
                endTime: toDay
            },
            showCustomModal: false,
            errorAlert: false,
            notData: false,
            isRefresh: false,
            refreshMes: '',
            searchItem: '',
            headImg: '',
            imgFiles: '',
            imgName: '',
            imagesItem: [],
            token: '',
            domain: 'http://res.bbtxsj.com/'
        }
    },
    ready(){
        _init_area();   //js实现全国三级城市联动select选择
        this.activity(0, null);
    },
    components: {
        navbarComponent,
        "loading-component": {
            template: loadingComponent.template,
            props: {
                mes:''
            }
        },
        modal
    },
    methods: {
        getToken(){
            var self = this;
            if(!_isTokenUpdata){
                self.token = _isToken;
                return;
            }
            _isToken = [];
            var parmas = {
                m: 'getToken'
            };
            this.$httpGet('qiniu', parmas, function(code, data){
                if(code == 0){
                    self.token = data.response;
                    _isToken = data.response;
                    _isTokenUpdata = false;
                    self.uploadImages();
                    self.uploadHeadImg();
                }
            });
        },
        activity(page, name){
            var self = this;
            if(!_isActivityUpdate){
                self.act = _isActivity;
                if(_isActivity.datas.length == 0){
                    self.notData = true;
                }
                return;
            }
            _isActivity = [];
            var params = {
                m: 'list',
                fetchCount: 10,
                page: page,
                type: 1
            };
            if(name != null){
                params['name'] = name;
            }
            this.$httpGet('activity', params, function(code, data){
                if(code == 0){
                    var result = data.response;
                    var pageItem = [];
                    for(var i = 0; i < data.response.pageCount; i++){
                        if(data.response.page == i){
                            pageItem.push({active: 'active', pageNum: i})
                        }else{
                            pageItem.push({active: '', pageNum: i})
                        }
                    }
                    result['pageItem'] = pageItem;
                    self.act = result;
                    _isActivity = result;
                    if(_isActivity.datas.length == 0){
                        self.notData = true;
                    }
                    _isActivityUpdate = false;
                }else{}
            });
        },
        search(name){
            _isActivityUpdate = true;
            this.activity(0, name);
        },
        check(){
            this.getToken();
            uploader = null;
            this.headImg = '';
            this.imagesItem = '';
            this.showCustomModal = true;
        },
        next(fenye, item){
            var pageNumber = item.page;
            var pageCount = item.pageCount;
            if(fenye == 'Next'){
                if(pageCount > pageNumber + 1){
                    this.activity(pageNumber + 1, null);
                }
            }else if(fenye == 'Previous'){
                if(pageNumber > 0){
                    this.activity(pageNumber - 1, null);
                }
            }else if(fenye == 'pageNum'){
                this.activity(item, null);
            }
        },
        uploadHeadImg(){
            console.log(true);
            var self = this;
            createUploader('acTheadfiles', 'acTcontainer', _isToken, function(code, domain, res){
                if(code == 0){
                    self.headImg = domain + res.key;
                }else if(code == 1){
                    console.log(domain)
                }
            });
        },
        uploadImages(){
            console.log(true);
            var self = this;
            createUploader('acTpickfiles', 'acTfileContainer', _isToken, function(code, domain, res){
                if(code == 0){
                    _isImages.push(domain + res.key);
                    self.imagesItem = _isImages;
                }else if(code == 1){
                    console.log(domain);
                }
            });
        },
        submit(item, img, images){
            if(!isClick){
                return;
            }
            isClick = false;
            this.isRefresh = true;
            this.refreshMes = '正在提交';
            if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
                return;
            }
            var self = this;
            var dataCheck = '';
            for(var i in item){
                if(item[i] === undefined  || item[i] == null || item[i] == ''){
                    dataCheck = false;
                }else{
                    dataCheck = true;
                }
            }
            if(dataCheck == false){
                alert('请确保信息填写完整！');
                return;
            }
            if(img == '' || img == null || img === undefined){
                alert('请上传活动封面图！');
                return;
            }
            if(dataCheck == false){
                alert('请确保信息填写完整！');
                return;
            }
            if(images == '' || images == null || images === undefined || images.length == 0){
                alert('请上传图片组！');
                return;
            }
            var desc = item.desc.replace(/\n/, "<br>");  // 获取文本域内所有换行符
            var actAddress = item.province + item.city + item.county + item.address;
            this.$amapGeocode(actAddress, item.city, function(code, data){
                var location = data.location;
                var i = location.indexOf(",");
                var lng = location.substring(0, i);
                var lat = location.substring(i + 1);
                //var lng  = data.longitude;
                //var lat = data.latitude;
                var params = {
                    "m":"addActivity",
                    "userID": localStorage.getItem('userID'),
                    "session": localStorage.getItem('session'),
                    "activityID": self.$route.query.activityID,
                    "name": item.name,
                    "address": actAddress,
                    "location":{
                        "longitude": lng,
                        "latitude": lat
                    },
                    "phone": item.phone,
                    "price": item.price,
                    "type": item.type,
                    "signUpTime": item.signUpTime,    //报名截至日期
                    "startTime": item.startTime,
                    "endTime": item.endTime,
                    "number": item.number, //最大人数,
                    "images": images,
                    "headImage": img,
                    "desc": desc
                };
                self.$httpPost('activity', params, function (code, data) {
                    if(code == 0){
                        self.showCustomModal = false;
                        _isActivityUpdate = true;
                        self.activity(0 ,null);
                    }else{
                        alert('error!')
                    }
                    self.isRefresh = false;
                    _isImages = [];
                    isClick = true;
                });
            });
        }
    },
    filters: {
        'dateFilter': function(date){
            if(date == null || date === undefined){
                return 'null'
            }else{
                var value = new Date(date);
                return moment(value).format("YYYY-MM-DD");
            }
        },
        'imageFilter': function(img){
            if(img === undefined || img == '' || img == null){
                return '<i class="glyphicon glyphicon-picture"></i>';
            }else{
                return '<img src="' + img +'" />'
            }
        }
    }
};
