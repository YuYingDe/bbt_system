/**
 * Created by YYD on 4/12/16.
 */
'use strict';
//var ExtractTextPlugin = require("extract-text-webpack-plugin");
//var webpack = require('webpack');
var path = require("path");

module.exports = {
    entry: "./dist/mko-router.js",
    output: {
        path: path.join(__dirname, "build"),
        publicPath:"build/",
        filename: "build.js",
        chunkFilename: "[name].chunk.js"
    },
    module: {
        //vue-loader
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'url',
                query: {
                    limit: 2048,
                    name: '[name].[ext]?[hash]'
                }
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?.*)?$/,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [
        //new webpack.ProvidePlugin({
        //    'window.Vue': 'vue'
        //})
        //new webpack.optimize.UglifyJsPlugin({
        //    compress: {
        //        //supresses warnings, usually from module minification
        //        warnings: false
        //    }
        //})
    ]
};