/**
 * Created by YYD on 5/6/16.
 */
//Router
var Vue = require('vue');
var VueRouter = require('vue-router');
Vue.use(VueRouter);

var App = Vue.extend({});

var router = new VueRouter({
    hashbang: false,
    history: true
});

router.map({
    '/': {
        component: require('./../template/home.vue')
    },
    '/org': {
        component: require('./../template/organization.vue')
    },
    '/statistics': {
        component: require('./../template/statistics.vue')
    },
    '/act': {
        component: require('./../template/activity.vue')
    },
    '/orgDetails': {
        component: require('./../template/organizationDetails.vue')
    },
    '/actDetails': {
        component: require('./../template/activityDetails.vue')
    },
    '/setting': {
        component: require('./../template/setting.vue')
    },
    '/login': {
        component: require('./../template/login.vue')
    },
    '/customerDetails': {
        component: require('./../template/customerDetails.vue')
    },
    '/studentDetails': {
        component: require('./../template/studentDetails.vue')
    }
});
router.beforeEach(function (transition) {
    if(transition.to.path === '/login'){
        transition.next();
    }else{
        if(!localStorage.getItem('session') || !localStorage.getItem('userID')){
            transition.redirect('/login');
        }else{
            transition.next()
        }
    }
});
router.start(App, '#app');