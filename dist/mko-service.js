/**
 * Created by YYD on 5/6/16.
 */
"use strict";

var Vue = require('vue');
var  VueResource = require('vue-resource');
Vue.use(VueResource);

;(function () {
    var bbt_key = 'f632af8b1ebbceb912fe24e3b7ad2202';
    var vue; // lazy bind
    var baseURL = 'http://120.24.165.155:8083/';
    //var baseURL = 'http://10.0.1.4:8083/';
    var asyncData = {
        created: function (){
            if (!vue) {
                console.warn('[vue-async-data] not installed!');
                return;
            }
        },
        compiled: function () {

        },
        methods: {
            $httpGet: function (service, params, cb){
                // GET request
                var queryArray = new Array();
                for(var key in params)
                    queryArray.push(key + "=" + params[key]);
                var queryString = queryArray.join("&");
                var queryURL = baseURL + service + '?' + queryString;
                console.log(queryURL);
                this.$http({
                    url: queryURL,
                    method: 'GET'
                }).then(function (result) {
                    var data = result.data;
                    if(data.code == 0){
                        cb(0, data);
                    }else{
                        cb(1, data);
                    }
                }, function (error) {
                    cb(-1, {});// error callback
                });
            },
            $httpPost: function (service, params, cb){
                // POST request
                var queryURL = baseURL + service;
                this.$http({
                    url: queryURL,
                    data: params,
                    method: 'POST'
                }).then(function (result) {
                    var data = result.data;
                    if(data.code == 0){
                        cb(0, data);
                    }else{
                        cb(1, data);
                    }
                }, function (error) {
                    cb(-1, {});// error callback
                });
            },
            //Geocoding APIWeb服务API --百度地图API
            $amapGeocode(address, cityname, cb){
                // GET request
                var baiduBaseUrl = 'http://restapi.amap.com/v3/geocode/geo?';
                var params = {
                    address: address,
                    key: bbt_key,
                    city: cityname
                };
                var queryArray = new Array();
                for(var key in params)
                    queryArray.push(key + "=" + params[key]);
                var queryString = queryArray.join("&");
                var queryURL = baiduBaseUrl + queryString;
                console.log(queryURL);
                this.$http({
                    url: queryURL,
                    method: 'GET',
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                    //var location =  {"longitude": 113.952441, "latitude": 22.553786};
                    cb(0, result.data.geocodes[0]);
                }, function (error) {
                    cb(-1, {});// error callback
                });
            },
            $qiniuUpload: function (params, cb){
                // POST request
                var queryURL = 'http://up.qiniu.com';
                this.$http({
                    url: queryURL,
                    data: params,
                    method: 'POST',
                    headers: {'Content-Type': 'multipart/form-data'}
                }).then(function (result) {
                    var data = result.data;
                    if(data.code == 0){
                        cb(0, data);
                    }else{
                        cb(1, data);
                    }
                }, function (error) {
                    cb(-1, {});// error callback
                });
            }
        }
    };

    var api = {
        mixin: asyncData,
        install: function (Vue, options) {
            vue = Vue
            Vue.options = Vue.util.mergeOptions(Vue.options, asyncData)
        }
    }

    if(typeof exports === 'object' && typeof module === 'object') {
        module.exports = api
    } else if (typeof window !== 'undefined') {
        window.MyVueAsyncData = api
        Vue.use(api);
    }
})();